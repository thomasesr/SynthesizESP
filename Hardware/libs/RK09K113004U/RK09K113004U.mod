PCBNEW-LibModule-V1  2019-09-13 17:01:56
# encoding utf-8
Units mm
$INDEX
RK09D1130C3C
$EndINDEX
$MODULE RK09D1130C3C
Po 0 0 0 15 5d7bbd74 00000000 ~~
Li RK09D1130C3C
Cd RK09D1130C3C
Kw Variable Resistor
Sc 0
At STD
AR 
Op 0 0 0
T0 2.5 -6.9 1.27 1.27 0 0.254 N V 21 N "VR**"
T1 2.5 -6.9 1.27 1.27 0 0.254 N I 21 N "RK09D1130C3C"
DS -2.4 -12.4 7.4 -12.4 0.2 24
DS 7.4 -12.4 7.4 -1.4 0.2 24
DS 7.4 -1.4 -2.4 -1.4 0.2 24
DS -2.4 -1.4 -2.4 -12.4 0.2 24
DS -3.8 -12.8 8.8 -12.8 0.1 24
DS 8.8 -12.8 8.8 1.2 0.1 24
DS 8.8 1.2 -3.8 1.2 0.1 24
DS -3.8 1.2 -3.8 -12.8 0.1 24
DS -2.4 -1.4 -2.4 -1.1 0.2 24
DS -2.4 -1.1 7.4 -1.1 0.2 24
DS 7.4 -1.1 7.4 -1.5 0.2 24
DS -2.4 -5 -2.4 -1.1 0.1 21
DS -2.4 -1.1 7.4 -1.1 0.1 21
DS 7.4 -1.1 7.4 -5 0.1 21
DS -2.4 -8.8 -2.4 -12.4 0.1 21
DS -2.4 -12.4 7.4 -12.4 0.1 21
DS 7.4 -12.4 7.4 -8.8 0.1 21
DS -2.4 -1.4 7.4 -1.4 0.1 21
$PAD
Po 0 0
Sh "1" C 1.8 1.8 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 2.5 0
Sh "2" C 1.8 1.8 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 5 0
Sh "3" C 1.8 1.8 0 0 900
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po -1.9 -7
Sh "4" C 3.4 3.4 0 0 900
Dr 2.2 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$PAD
Po 6.9 -7
Sh "5" C 3.4 3.4 0 0 900
Dr 2.2 0 0
At STD N 00E0FFFF
Ne 0 ""
$EndPAD
$EndMODULE RK09D1130C3C
$EndLIBRARY
