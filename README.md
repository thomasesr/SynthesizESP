**SynthesizESP**

Open Source ESP32 Synthesizer with MIDI Interfaces

**Project Structure:**


-Hardware       (KiCAD files) 

    -libs       (KiCADlibraryfiles)
        -3D     (3Drenderingsforcomponents)
    -assets     (Reference designs and assets)
    -Gerber     (gerber files for manufacturing)
    -PDF        (PDF Files of Schematics and board layout)


-Software       (ESPIDF/ADF project files)

    -src        (main source files)
    -.vscode    (VSCode project configuration files)
    -libs       (external library files)
    -include
    -platformio.ini (Project file for PlatformIO VSCode Extension)


> This project is based on the One Lone Coder Synthesizer Source Code ported to work on a ESP32 embbeded Device 

**About The Hardware:**
The ESP32 has 2 cores that run at 260MHz, about 300Kb of free RAM and 4MB of PSRAM, it has built in WiFi, 3 UART interfaces, 1 UART will be used for debbuging and flashing 1 for MIDI instrument interfacing and 1 for HMI Display interface. It also has SD_MMC SD card support and Bluetooth, 4 SPI interfaces (one for internal Flash, one for PSRAM, and there are 2 free SPI interfaces) It has a bunch of GPIO, I2C and other stuff. It has PWM drivers and a IR Remote Controll driver that will be used to generate Dallas/Maxim 1Wire Signal timmings to connect to IO Port Expanders and ADCs built on a Interfacing Board filled with buttons and potentiometers to configure the Synthesizers instruments and wave functions and change the sound characteristics. 

**1Wire:**
IO18, IO19 and IO21 are each connected to a MAXIM DS2450S 1wire 4 Channel Analog to Digital Converters. Each Channel of each ADC is connected to a 10K potentiometer Voltage Divider which will read between 0V and 5V. The Lib will send a 1W reset pulse, wait for a presence pulse, send a Convert Byte and a Read Scratchpad to get the bytes that include the converted voltage value. The value of each Channel is stored in memory and used as a variables to change the characteristics of the Synthesizer. The time taken to make the AD conversion at 12bit (5V/4096) for all 4 channels is about 4ms. Even faster if a lower resolution is chosen.

IO22 is being used as a 1Wire Bus connected to 2 or more MAXIMs DS2408S in Overdrive mode Each. The DS2408S has 8 Channels IO connected to pull up resistors and LEDs with buttons that short the pins to ground. The status of the buttons are Booleans that will be stored in memory to change the characteristics of the Synthesizers instruments and wave forms. 

IO23 is a 1Wire Bus connected to 1 MAXIM DS2408S in overdrive mode. This particular one is connected to 2 rotary encoders with push buttons that will be used to navigate the menus of the Synthesizer.

**UART Interfaces:**
TXD0 HMI Display / ROM Flash
RXD0 HMI Display /Serial Monitor 
TX1 IO33 MIDI OUT 
RX1 IO32 MIDI IN 

**I2S Interface:**
The ESP32 uses IO25 IO26 and IO27 to connect to two Maxim Integrated MAX98357A I2S decoder DAC with Class D Amplifiers in parallel. Each one will decode the I2S signal from the ESP32 into the Left and Right Audio Channels and the IO05 is used to disable the audio output from MAX98357A ICs when the Bluetooth audio out is enabled.

**SD_MMC SD Card:**
The ESP32 has a SDMMC interface available in the pins IO12, IO13, IO14, IO15, IO02 and IO04. The File system will be used to read and write instrument, configurations and MIDI files.

**About the Software:**
Beyond the OLC Synthesizer Code the following Libs are being used:

-esp32owb   (Maxim One Wire Busdriver for ESP32)
-ESP-ADF    (Expressif Audio Libs)
-MIDI

**The Software Has several tasks:**

**1 Wire Task:** 
This task initializes 5 1-wire busses in overdrive mode and get the IO status of the buttons and rotary encoders as well as get the ADC status and put them in memory for the Synthesizer to use as inputs to change the sound characteristics. 

**A2DP SRC Task: **
This task is the one responsible to send Digital Audio signals (A2DP SRC) via Bluetooth to a A2DP SYNC Capable Device (Bluetooth Speaker).

**FreeRTOS:**
Base OS Task

**OLC Synth:**
Synthesizer Main Task

**MIDI:**
This task uses one of the UART interfaces to receive bytes from MIDI instruments, the bytes received represent MIDI Messages that includes commands like Key stroke start (note on event), Key stroke end (note off event), Channel, Channel preasure, Pitch Bend and etc. This task is also used to send MIDI messages to other synthesizers from MIDI Files located in the SD Card File System.